﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

using  Calidad.Models;
using Calidad.Interface;
using Calidad.Interface.Interfaces;
using Calidad.Models.Entities;
using ExamenT2Calidad.Controllers;
using Moq;
using NUnit.Framework;
using Validators.Validadores;

namespace Calidad.Test.Controllers
{
   
   [TestFixture]
   public class PosoControllerTesting
    {
       [Test]
       public void RetornaLaVista()
       {
           var mock = new Mock<InterfacePozo>();
           mock.Setup(o => o.All()).Returns(new List<Pozo>());
           mock.Setup(o => o.ByQueryAll("")).Returns(new List<Pozo>());

           var controller = new PozoController(mock.Object, null);
           var view = controller.Index();

           Assert.IsInstanceOf(typeof(ViewResult), view);
           Assert.AreEqual("Inicio", view.ViewName);
           Assert.IsInstanceOf(typeof(List<Pozo>), view.Model);
       }
       [Test]
       public void TestCreateReturnView()
       {
           var controller = new PozoController(null, null);

           var view = controller.Create();

           Assert.IsInstanceOf(typeof(ViewResult), view);
           Assert.AreEqual("Create", view.ViewName);
       }
       
        [Test]
        public void TestPostCreateOKReturnRedirect()
        {
            var pozo = new Pozo();

            var repositoryMock = new Mock<InterfacePozo>();

            repositoryMock.Setup(o => o.Store(new Pozo()));

            var validatorMock = new Mock<PozoValidators>();

            validatorMock.Setup(o => o.Pass(pozo)).Returns(true);

            var controller = new PozoController(repositoryMock.Object, validatorMock.Object);

            var redirect = (RedirectToRouteResult)controller.Create(pozo);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), redirect);
        }

        [Test]
        public void TestPostCreateCallStoreMethodFromRepository()
        {
            var pozo = new Pozo();

            var repositoryMock = new Mock<InterfacePozo>();

            var validatorMock = new Mock<PozoValidators>();

            validatorMock.Setup(o => o.Pass(pozo)).Returns(true);

            repositoryMock.Setup(o => o.Store(pozo));

            var controller = new PozoController(repositoryMock.Object, validatorMock.Object);

            var redirect = controller.Create(pozo);

            repositoryMock.Verify(o => o.Store(pozo), Times.Once());

        }

        [Test]

        public void TestPostCreateReturnViewWithErrorsWhenValidationFail()
        {
            var pozo = new Pozo { };

            var mock = new Mock<PozoValidators>();

            mock.Setup(o => o.Pass(pozo)).Returns(false);

            var controller = new PozoController(null, mock.Object);

            var view = controller.Create(pozo);

            Assert.IsInstanceOf(typeof(ViewResult), view);
        }


        [Test]
        public void TestAsignarUnaPersonaPozo()
        {
            var pozo = new Pozo { };

            var mock = new Mock<InterfacePozo>();
            var Persona = new Personas {Id = 1, Apellidos = "tanta"};
            mock.Setup(o => o.All()).Returns(new List<Pozo>());
            mock.Setup(o => o.ByQueryAll("")).Returns(new List<Pozo>());
            mock.Setup(o => o.AsignarUnaPersonaPozo(pozo,Persona)).Returns(new Pozo());
            var mockv = new Mock<PozoValidators>();

            mockv.Setup(o => o.Pass(pozo)).Returns(false);
            var controller = new PozoController(mock.Object, mockv.Object);
            var view = controller.AsignarUnaPersonaPozo(pozo,Persona);
            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("AsignarUnaPersonaPozo",view.ViewName);

        }

    }
}
