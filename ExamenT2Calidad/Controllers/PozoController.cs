﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Calidad.Interface.Interfaces;
using Calidad.Models.Entities;
using Validators.Validadores;

namespace ExamenT2Calidad.Controllers
{
    public class PozoController : Controller
    {
        //
        // GET: /Pozo/

        private InterfacePozo repository;
        private PozoValidators validator;

          public PozoController(InterfacePozo repository, PozoValidators validator)
        {
            this.repository = repository;
            this.validator = validator;
        }

        [HttpGet]
          public ViewResult Index(String query = "")
          {
              var datos = repository.ByQueryAll(query);
              return View("Inicio",datos);
          }

          [HttpGet]
          public ViewResult Create()
          {
              return View("Create");
          }

          [HttpPost]
          public ActionResult Create(Pozo pozo)
          {

              if (validator.Pass(pozo))
              {
                  repository.Store(pozo);

                  TempData["UpdateSuccess"] = "se envio correctamente";

                  return RedirectToAction("Index");
              }

              return View("Inicio", pozo);

          }

          [HttpPost]
          public ViewResult AsignarUnaPersonaPozo(Pozo pozo,Personas personas)
          {
              var a = repository.AsignarUnaPersonaPozo(pozo, personas);
              return View("AsignarUnaPersonaPozo");

          }

    }
}
