﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calidad.Models.Entities
{
    public class Pozo
    {
       // public Int32 Id { get; set; }
        public String Nombre { get; set; }
        public String Porpietario { get; set; }
        public String Ubicacion { get; set; }
        public String CordenadaEste { get; set; }
        public String CoordenadaOeste { get; set; }
    }
}
