﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calidad.Models.Entities;

namespace Calidad.Interface.Interfaces
{
    public interface InterfacePozo
    {
        List<Pozo> All();
        void Store(Pozo pozo);
        List<Pozo> ByQueryAll(string query);
        Pozo AsignarUnaPersonaPozo(Pozo pozo, Personas personas);
       

    }
}
